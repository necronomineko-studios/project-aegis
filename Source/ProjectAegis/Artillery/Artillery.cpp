// Copyright Necronomineko Studio


#include "Artillery.h"
#include "Turret.h"
#include "Barrel.h"
#include "Camera/CameraComponent.h"


void AArtillery::Initialise(UTurret* Turret, UBarrel* Barrel, UCameraComponent* Camera)
{
	this->Turret = Turret;
	this->Barrel = Barrel;
	this->Camera = Camera;
}

void AArtillery::RotateTurret(float Speed)
{
	if (!Turret) return;

	Turret->Rotate(Speed);
}

void AArtillery::ElevateBarrel(float Speed)
{
	if (!Barrel) return;

	Barrel->Elevate(Speed);
}

void AArtillery::FirePrimary(float Rate)
{
	if (!Barrel || FMath::IsNearlyZero(Rate)) return;

	Barrel->Fire(Rate);
}

void AArtillery::FireSecondary()
{

}