// Copyright Necronomineko Studio

#pragma once

// Default includes
#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Artillery.generated.h"

/**
 * Represents the base Artillery which can be controlled by the player.
 */
UCLASS()
class PROJECTAEGIS_API AArtillery : public APawn
{
	GENERATED_BODY()

public:
	void RotateTurret(float Speed);
	void ElevateBarrel(float Speed);
	void FirePrimary(float Rate);
	void FireSecondary();

protected:
	UFUNCTION(BlueprintCallable, Category = "Setup")
	void Initialise(class UTurret* Turret, class UBarrel* Barrel, class UCameraComponent* Camera);

private:
	UPROPERTY(VisibleInstanceOnly, Category = "Components")
	class UTurret* Turret = nullptr;
	UPROPERTY(VisibleInstanceOnly, Category = "Components")
	class UBarrel* Barrel = nullptr;
	UPROPERTY(VisibleInstanceOnly, Category = "Components")
	class UCameraComponent* Camera = nullptr;


};
