// Copyright Necronomineko Studio


#include "ArtilleryController.h"
#include "Artillery.h"
#include "Kismet/GameplayStatics.h"

void AArtilleryController::BeginPlay()
{
	Super::BeginPlay();

	auto ID = UGameplayStatics::GetPlayerControllerID(this);
	if (ID == FIRST_PLAYER_ID)
	{
		LeftArtillery = Cast<AArtillery>(GetPawn());
		auto RightController = UGameplayStatics::CreatePlayer(this, SECOND_PLAYER_ID);
		if(RightController && RightController->GetPawn())
		{
			RightArtillery = Cast<AArtillery>(RightController->GetPawn());
		}

		// Something has gone wrong if the first controller doesn't have two artilleries.
		ensure(LeftArtillery && RightArtillery);
		
		SetupInput();
	}
}

void AArtilleryController::SetupInput()
{
	auto ID = UGameplayStatics::GetPlayerControllerID(this);
	if (ID == FIRST_PLAYER_ID && InputComponent && LeftArtillery && RightArtillery)
	{
		InputComponent->BindAxis("Rotate LEFT", LeftArtillery, &AArtillery::RotateTurret);
		InputComponent->BindAxis("Elevate LEFT", LeftArtillery, &AArtillery::ElevateBarrel);
		InputComponent->BindAxis("Fire Primary LEFT", LeftArtillery, &AArtillery::FirePrimary);
		InputComponent->BindAction("Fire Secondary LEFT", IE_Pressed, LeftArtillery, &AArtillery::FireSecondary);

		InputComponent->BindAxis("Rotate Right", RightArtillery, &AArtillery::RotateTurret);
		InputComponent->BindAxis("Elevate RIGHT", RightArtillery, &AArtillery::ElevateBarrel);
		InputComponent->BindAxis("Fire Primary RIGHT", RightArtillery, &AArtillery::FirePrimary);
		InputComponent->BindAction("Fire Secondary RIGHT", IE_Pressed, RightArtillery, &AArtillery::FireSecondary);
	}
}