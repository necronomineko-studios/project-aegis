// Copyright Necronomineko Studio

#pragma once

// Default includes
#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ArtilleryController.generated.h"

// Forward declarations
class AArtillery;

/**
 * A player controller that controls artillery pawns.
 */
UCLASS()
class PROJECTAEGIS_API AArtilleryController : public APlayerController
{
	GENERATED_BODY()

	static constexpr int32 FIRST_PLAYER_ID = 0;
	static constexpr int32 SECOND_PLAYER_ID = 1;
	
public:

protected:
	virtual void BeginPlay() override;

private:
	void SetupInput();

	UPROPERTY(VisibleAnywhere, Category = "Artilleries")
	AArtillery* LeftArtillery = nullptr;
	UPROPERTY(VisibleAnywhere, Category = "Artilleries")
	AArtillery* RightArtillery = nullptr;
};
