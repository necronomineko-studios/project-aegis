// Copyright Necronomineko Studio


#include "Barrel.h"
#include "Engine/World.h"
#include "Projectile.h"
#include "UnrealMath.h"

void UBarrel::Elevate(float RelativeSpeed)
{
	RelativeSpeed = FMath::Clamp(RelativeSpeed, -1.f, 1.f);
	auto PitchDelta = RelativeSpeed * MaxDegreesPerSecond * GetWorld()->DeltaTimeSeconds;
	auto PitchSum = RelativeRotation.Pitch + PitchDelta;
	auto PitchClamped = FMath::Clamp(PitchSum, MinElevationDegrees, MaxElevationDegrees);
	SetRelativeRotation(FRotator(PitchClamped, 0.f, 0.f));
}