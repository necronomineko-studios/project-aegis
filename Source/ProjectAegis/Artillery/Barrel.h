// Copyright Necronomineko Studio

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "Barrel.generated.h"

/**
 * Represents the base primary barrel which specific primary weapons inherit from.
 */
UCLASS( Blueprintable, BlueprintType, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent) )
class PROJECTAEGIS_API UBarrel : public UStaticMeshComponent
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "Transform")
	void Elevate(float RelativeSpeed);

	UFUNCTION(BlueprintCallable, Category = "Fire")
	virtual void Fire(float Rate){ UE_LOG(LogTemp, Warning, TEXT("This fire method should be overridden. %s"), *GetName()) };

private:
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float MinElevationDegrees = 0.f;
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float MaxElevationDegrees = 90.f;
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float MaxDegreesPerSecond = 23.f;
};
