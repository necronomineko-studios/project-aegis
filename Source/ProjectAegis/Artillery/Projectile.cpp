// Copyright Necronomineko Studio


#include "Projectile.h"
#include "Components\StaticMeshComponent.h"
#include "Engine\Public\TimerManager.h"
#include "Engine\World.h"
#include "GameFramework\DamageType.h"
#include "GameFramework\ProjectileMovementComponent.h"
#include "Kismet\GameplayStatics.h"
#include "Particles\ParticleSystemComponent.h"
#include "PhysicsEngine\RadialForceComponent.h"

AProjectile::AProjectile()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	CollisionMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("Collision Mesh"));
	SetRootComponent(CollisionMesh);
	CollisionMesh->SetNotifyRigidBodyCollision(true);
	CollisionMesh->SetVisibility(false);

	LaunchBlast = CreateDefaultSubobject<UParticleSystemComponent>(FName("Launch Blast"));
	LaunchBlast->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	ImpactBlast = CreateDefaultSubobject<UParticleSystemComponent>(FName("Impact Blast"));
	ImpactBlast->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	ImpactBlast->bAutoActivate = false;

	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(FName("Projectile Movement"));

	ExplosionForce = CreateDefaultSubobject<URadialForceComponent>(FName("Explosion Force"));
	ExplosionForce->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

void AProjectile::BeginPlay()
{
	Super::BeginPlay();

	if (!CollisionMesh) return;

	CollisionMesh->OnComponentHit.AddDynamic(this, &AProjectile::OnHit);
	GetWorldTimerManager().SetTimer(DestroyTimer, this, &AProjectile::OnTimerExpire, MaxLifetime, false);
}

void AProjectile::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	GetWorldTimerManager().ClearTimer(DestroyTimer);
}

void AProjectile::LaunchProjectile(FVector Velocity)
{
	ProjectileMovementComponent->Velocity = Velocity;
	ProjectileMovementComponent->Activate();
}

void AProjectile::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	if (LaunchBlast && ImpactBlast && ExplosionForce)
	{
		LaunchBlast->Deactivate();
		ImpactBlast->Activate();
		ExplosionForce->FireImpulse();

		SetRootComponent(ImpactBlast);
		if(CollisionMesh)
			CollisionMesh->DestroyComponent();

		UGameplayStatics::ApplyRadialDamage(this, ProjectileDamage, GetActorLocation(), ExplosionForce->Radius, UDamageType::StaticClass(), TArray<AActor*>());

		GetWorldTimerManager().ClearTimer(DestroyTimer);
		GetWorldTimerManager().SetTimer(DestroyTimer, this, &AProjectile::OnTimerExpire, DestroyDelayOnHit, false);
	}
	else
	{
		Destroy();
	}
}

void AProjectile::OnTimerExpire()
{
	Destroy();
}

