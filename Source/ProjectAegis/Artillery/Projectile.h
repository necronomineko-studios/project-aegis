// Copyright Necronomineko Studio

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

UCLASS()
class PROJECTAEGIS_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectile();

	void LaunchProjectile(FVector Velocity);

private:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);
	void OnTimerExpire();

	UPROPERTY(VisibleAnywhere, Category = "Components")
	class UProjectileMovementComponent* ProjectileMovementComponent = nullptr;
	UPROPERTY(VisibleAnywhere, Category = "Components")
	UStaticMeshComponent* CollisionMesh = nullptr;
	UPROPERTY(VisibleAnywhere, Category = "Components")
	class UParticleSystemComponent* LaunchBlast = nullptr;
	UPROPERTY(VisibleAnywhere, Category = "Components")
	class UParticleSystemComponent* ImpactBlast = nullptr;
	UPROPERTY(VisibleAnywhere, Category = "Components")
	class URadialForceComponent* ExplosionForce = nullptr;

	/** Max lifetime in seconds, destroyed after this time. */
	UPROPERTY(EditDefaultsOnly, Category = "Lifetime")
	float MaxLifetime = 10.f;
	/** Time after hit to destroy the projectile, letting the effect play. */
	UPROPERTY(EditDefaultsOnly, Category = "Lifetime")
	float DestroyDelayOnHit = 5.f;
	/** Amount of damage this projectile deals. */
	UPROPERTY(EditDefaultsOnly, Category = "Damage")
	float ProjectileDamage = 100.f;

	UPROPERTY()
	FTimerHandle DestroyTimer;
};
