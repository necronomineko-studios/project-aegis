// Copyright Necronomineko Studio


#include "Turret.h"
#include "UnrealMath.h"

void UTurret::Rotate(float RelativeSpeed)
{
	RelativeSpeed = FMath::Clamp(RelativeSpeed, -1.f, 1.f);
	auto RotationYawChange = RelativeSpeed * MaxDegreesPerSecond * GetWorld()->DeltaTimeSeconds;
	auto DeltaRotation = FRotator(0.f, RotationYawChange, 0.f);
	AddRelativeRotation(DeltaRotation);
}