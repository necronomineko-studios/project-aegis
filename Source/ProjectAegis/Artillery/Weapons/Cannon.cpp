// Copyright Necronomineko Studio


#include "Cannon.h"
#include "Engine/World.h"
#include "../Projectile.h"

void UCannon::Fire(float Rate)
{
	float Delay = FMath::Lerp(MaxFireDelay, MinFireDelay, Rate);
	if(GetWorld()->TimeSeconds >= LastFireTime + Delay)
	{
		GetWorld()->SpawnActor<AProjectile>(ProjectileBlueprint, GetSocketLocation(FName("Projectile")), GetSocketRotation(FName("Projectile")));
		LastFireTime = GetWorld()->TimeSeconds;
	}
}