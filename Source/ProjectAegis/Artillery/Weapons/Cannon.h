// Copyright Necronomineko Studio

#pragma once

#include "CoreMinimal.h"
#include "Artillery/Barrel.h"
#include "Cannon.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PROJECTAEGIS_API UCannon : public UBarrel
{
	GENERATED_BODY()
	
public:
	virtual void Fire(float Rate) override;

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	TSubclassOf<class AProjectile> ProjectileBlueprint = nullptr;

private:
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float MinFireDelay = 0.1f;
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float MaxFireDelay = 1.0f;

	float LastFireTime = 0.f;
};
