// Copyright Necronomineko Studio


#include "StarGenerator.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"

// Sets default values
AStarGenerator::AStarGenerator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	HISM = CreateDefaultSubobject<UHierarchicalInstancedStaticMeshComponent>(TEXT("HISM"));
	if (HISM)
	{
		HISM->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		HISM->SetMobility(EComponentMobility::Static);
		SetRootComponent(HISM);
	}
}

void AStarGenerator::GenerateStars() const
{
	if (!HISM) return;

	HISM->ClearInstances();

	for (int32 Count = 0; Count < StarCount; Count++)
	{
		FVector Scale = RandomStarScale();
		FVector Location = RandomStarLocation();
		FTransform Transform = FTransform(FRotator::ZeroRotator, Location, Scale);
		HISM->AddInstance(Transform);
	}
}

void AStarGenerator::ClearStars() const
{
	if (!HISM) return;

	HISM->ClearInstances();
}

FVector AStarGenerator::RandomStarScale() const 
{
	const float Alpha = RandomExponentialDistribution01(StarScaleBase, StarScaleExponent);
	const float Scale = FMath::Lerp(MinStarScale, MaxStarScale, Alpha);

	return FVector(Scale);
}

FVector AStarGenerator::RandomStarLocation() const
{
	FVector Direction = FVector::ZeroVector;

	// Logic for random unit vector borrowed from FMath::VRand(), Z-axis logic changed.
	float L;
	do
	{
		Direction.X = FMath::FRandRange(-1.f, 1.f);
		Direction.Y = FMath::FRandRange(-1.f, 1.f);
		Direction.Z = RandomExponentialDistribution01(StarPitchBase, StarPitchExponent);
		L = Direction.SizeSquared();
	} while (L > 1.0f || L < KINDA_SMALL_NUMBER);
	Direction *= 1.0f / FMath::Sqrt(L);

	const FVector Location = Direction.GetUnsafeNormal() * FMath::FRandRange(MinStarDistance, MaxStarDistance);
	return Location;
}

float AStarGenerator::RandomExponentialDistribution01(const float Base, const float MaxExponent) const
{
	const float RandomExponentResult = FMath::Pow(Base, FMath::FRandRange(0.f, MaxExponent)) - 1.f; // Minus one to give lowest value of 0.
	const float MaxExponentResult = FMath::Pow(Base, MaxExponent) - 1.f; // Minus one to account for lowest value of 0.
	const float NormalisedRandomExponentResult = RandomExponentResult / MaxExponentResult;

	return NormalisedRandomExponentResult * GetAlternateSign();
}

int8 AStarGenerator::GetAlternateSign() const
{
	static int8 Sign = 1;
	return Sign *= -1;
}
