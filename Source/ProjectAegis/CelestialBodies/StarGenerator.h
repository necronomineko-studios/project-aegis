// Copyright Necronomineko Studio

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "StarGenerator.generated.h"

/**
 * Class to generate stars, should be inherited by blueprint.
 */
UCLASS()
class PROJECTAEGIS_API AStarGenerator : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AStarGenerator();

protected:
	/** Populates the HISM with generated stars. */
	UFUNCTION(BlueprintCallable, CallInEditor, Category = "Instance")
	void GenerateStars() const;

	/** Clears the HISM. */
	UFUNCTION(BlueprintCallable, CallInEditor, Category = "Instance")
	void ClearStars() const;

	/**
	 * @return Random star scale.
	 */
	UFUNCTION(BlueprintCallable, Category = "Random")
	FVector RandomStarScale() const;

	/**
	 * @return Random star location.
	 */
	UFUNCTION(BlueprintCallable, Category = "Random")
	FVector RandomStarLocation() const;

	/** 
	 * Function to generate a random pitch between -1.0 and 1.0 with an exponential distribution from the middle to the min and max values.
	 * @param Base - The base of the exponential equation.
	 * @param MaxExponent - The max exponent of the exponential equation.
	 * @return Random pitch between -1.0 and 1.0.
	 */
	UFUNCTION(BlueprintCallable, Category = "Random")
	float RandomExponentialDistribution01(const float Base, const float MaxExponent) const;

	/** Returns alternating sign (1 or -1) each time it's called. Not thread safe. */
	int8 GetAlternateSign() const;

private:
	/** Set Mesh and Material in blueprint. */
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Instance", meta = (AllowPrivateAccess = "true"))
	class UHierarchicalInstancedStaticMeshComponent* HISM = nullptr;

	/** Amount of stars to generate. */
	UPROPERTY(EditAnywhere, Category = "Instance", meta = (AllowPrivateAccess = "true"))
	int32 StarCount = 100000;

	/** Minimum scale for the stars. */
	UPROPERTY(EditAnywhere, Category = "Instance", meta = (AllowPrivateAccess = "true"))
	float MinStarScale = 8500.f;
	/** Maximum scale for the stars. */
	UPROPERTY(EditAnywhere, Category = "Instance", meta = (AllowPrivateAccess = "true"))
	float MaxStarScale = 20000.f;

	/** The base for the exponential distribution of star scale. */
	UPROPERTY(EditAnywhere, Category = "Instance", meta = (AllowPrivateAccess = "true"))
	float StarScaleBase = 2.f;
	/** The exponent for the exponential distribution of star scale. */
	UPROPERTY(EditAnywhere, Category = "Instance", meta = (AllowPrivateAccess = "true"))
	float StarScaleExponent = 5.f;

	/** Minimum distance for the stars. */
	UPROPERTY(EditAnywhere, Category = "Instance", meta = (AllowPrivateAccess = "true"))
	float MinStarDistance = 1000000000.f;
	/** Maximum distance for the stars. */
	UPROPERTY(EditAnywhere, Category = "Instance", meta = (AllowPrivateAccess = "true"))
	float MaxStarDistance = 1000000000.f;

	/** The base for the exponential distribution of star pitch. */
	UPROPERTY(EditAnywhere, Category = "Instance", meta = (AllowPrivateAccess = "true"))
	float StarPitchBase = 2.f;
	/** The exponent for the exponential distribution of star pitch. */
	UPROPERTY(EditAnywhere, Category = "Instance", meta = (AllowPrivateAccess = "true"))
	float StarPitchExponent = 5.f;
};
