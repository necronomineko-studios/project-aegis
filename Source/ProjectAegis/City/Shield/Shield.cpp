// Copyright Necronomineko Studio


#include "Shield.h"

float AShield::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	const float DamageToApply = FMath::Clamp(Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser), 0.f, CurrentHealth);

	CurrentHealth -= DamageToApply;
	if (CurrentHealth <= 0.f)
		BreakShield();

	return DamageToApply;
}

void AShield::BreakShield()
{
	// TODO: Do something proper here.

	// TODO: Determine if Destroy is the right way to go.
	Destroy();
}

