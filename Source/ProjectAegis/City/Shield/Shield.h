// Copyright Necronomineko Studio

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Shield.generated.h"

UCLASS()
class PROJECTAEGIS_API AShield : public AActor
{
	GENERATED_BODY()
	
public:	
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

protected:

private:
	void BreakShield();

	UPROPERTY(EditDefaultsOnly, Category = "Health")
	float CurrentHealth = 100.f;

};
