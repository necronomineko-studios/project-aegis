// Copyright Necronomineko Studio


#include "DebugDestroy.h"
#include "Engine/World.h"

// Sets default values for this component's properties
UDebugDestroy::UDebugDestroy()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called every frame
void UDebugDestroy::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	UE_LOG(LogTemp, Warning, TEXT("%f"), GetWorld()->TimeSeconds)

	if (GetWorld()->TimeSeconds > 5.f)
	{
		if (GetOwner()->Destroy())
		{
			UE_LOG(LogTemp, Warning, TEXT("Destroy Success"))
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Destroy Fail"))
		}
	}
}

