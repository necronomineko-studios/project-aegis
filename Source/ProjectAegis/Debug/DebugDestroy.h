// Copyright Necronomineko Studio

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "DebugDestroy.generated.h"

/* Class used to help debug the actor Destroy method not working. */
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PROJECTAEGIS_API UDebugDestroy : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UDebugDestroy();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

};
