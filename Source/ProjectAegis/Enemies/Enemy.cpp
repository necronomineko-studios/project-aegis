// Copyright Necronomineko Studio


#include "Enemy.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

float AEnemy::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	const float DamageToApply = FMath::Clamp(Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser), 0.f, CurrentHealth);
	
	CurrentHealth -= DamageToApply;
	if (FMath::IsNearlyEqual(CurrentHealth, 0.f))
		OnDeath();

	return DamageToApply;
}

void AEnemy::OnDeath()
{
	if(Destroy())
		UE_LOG(LogTemp, Warning, TEXT("Enemy destroyed"))
	else
		UE_LOG(LogTemp, Warning, TEXT("Enemy not destroyed"))
}
