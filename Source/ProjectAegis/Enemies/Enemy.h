// Copyright Necronomineko Studio

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Enemy.generated.h"

UCLASS()
class PROJECTAEGIS_API AEnemy : public APawn
{
	GENERATED_BODY()

public:
	AEnemy();

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

protected:
	virtual void OnDeath();

	UPROPERTY(EditDefaultsOnly, Category= "Health")
	float CurrentHealth = 100.f;
};
