// Copyright Necronomineko Studio



#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Engine/World.h"
#include "FirstPersonCharacter.h"
#include "FirstPersonController.h"
#include "GameFramework/InputSettings.h"
#include "PossessionIntermediary.h"

// Sets default values
AFirstPersonCharacter::AFirstPersonCharacter()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	// Set size for collision capsule
	Capsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule"));
	SetRootComponent(Capsule);
	Capsule->InitCapsuleSize(55.f, 96.0f);

	// Create a CameraComponent	
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(GetRootComponent());
	Camera->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	Camera->bUsePawnControlRotation = false;
}

// Called to bind functionality to input
void AFirstPersonCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Bind interaction event
	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &AFirstPersonCharacter::Interact);

	// Bind movement events
	PlayerInputComponent->BindAxis("Move Forward", this, &AFirstPersonCharacter::MoveForward);
	PlayerInputComponent->BindAxis("Move Right", this, &AFirstPersonCharacter::MoveRight);
	PlayerInputComponent->BindAxis("Turn", this, &AFirstPersonCharacter::Turn);
	PlayerInputComponent->BindAxis("Look Up", this, &AFirstPersonCharacter::LookUp);
}

void AFirstPersonCharacter::Interact()
{
	AFirstPersonController* FirstPersonController = Cast<AFirstPersonController>(GetController());
	if (FirstPersonController)
	{
		FHitResult Hit;
		if (FirstPersonController->LineTraceSingleByChannelFromCenterOfScreen(Hit, Reach, ECollisionChannel::ECC_Visibility))
		{
			UPossessionIntermediary* PossessionIntermediary = Hit.Actor->FindComponentByClass<UPossessionIntermediary>();
			if (PossessionIntermediary)
			{
				PossessionIntermediary->PossessPawn(FirstPersonController);
			}
		}
	}
}

void AFirstPersonCharacter::MoveForward(float Value)
{
	MoveInDirection(GetActorForwardVector(), Value);
}

void AFirstPersonCharacter::MoveRight(float Value)
{
	MoveInDirection(GetActorRightVector(), Value);
}

void AFirstPersonCharacter::MoveInDirection(FVector Direction, float Value)
{
	FVector MoveDelta = Direction * MovementSpeed * Value * GetWorld()->GetDeltaSeconds();
	FVector Destination = GetActorLocation() + MoveDelta;
	FHitResult* HitResult = new FHitResult();
	bool bMoveSuccessful = SetActorLocation(Destination, true, HitResult, ETeleportType::TeleportPhysics);

	if (!bMoveSuccessful)
	{
		FVector ProjectResult = FVector::VectorPlaneProject(MoveDelta, HitResult->ImpactNormal);
		SetActorLocation(GetActorLocation() + ProjectResult, true, nullptr, ETeleportType::TeleportPhysics);
	}
}

void AFirstPersonCharacter::Turn(float Value)
{
	FRotator RotationChange = FRotator::ZeroRotator;
	RotationChange.Yaw = TurnSpeed * Value;
	AddActorLocalRotation(RotationChange);
}

void AFirstPersonCharacter::LookUp(float Value)
{
	FRotator RotationChange = FRotator::ZeroRotator;
	RotationChange.Pitch = TurnSpeed * Value;
	Camera->AddLocalRotation(RotationChange);
}
