// Copyright Necronomineko Studio

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FirstPersonCharacter.generated.h"

UCLASS()
class PROJECTAEGIS_API AFirstPersonCharacter : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AFirstPersonCharacter();

protected:
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/** Interact with interactable objects */
	void Interact();

	/** Handles moving forward/backward */
	void MoveForward(float Value);

	/** Handles moving right/left */
	void MoveRight(float Value);

	/** Handles moving in a direction */
	void MoveInDirection(FVector Direction, float Value);

	/** Handles turning left/right */
	void Turn(float Value);

	/** Handles looking up/down */
	void LookUp(float Value);

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Capsule", meta = (AllowPrivateAccess = "true"))
	class UCapsuleComponent* Capsule = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* Camera = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Movement")
	float MovementSpeed = 100.f;

	UPROPERTY(EditDefaultsOnly, Category = "Movement")
	float TurnSpeed = 2.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Interaction", meta = (AllowPrivateAccess = "true"))
	float Reach = 100.f;
};
