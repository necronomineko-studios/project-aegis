// Copyright Necronomineko Studio


#include "Blueprint/UserWidget.h"
#include "Engine/World.h"
#include "FirstPersonController.h"
#include "GameFramework/Pawn.h"


void AFirstPersonController::BeginPlay()
{
	Super::BeginPlay();

	DefaultPawn = GetPawn();
	if (!DefaultPawn)
	{
		UE_LOG(LogTemp, Warning, TEXT("Player Controller found no default pawn."))
	}

	if (PlayerWidgetClass)
	{
		PlayerWidget = CreateWidget(this, PlayerWidgetClass, FName("Player Widget"));
		if (PlayerWidget)
		{
			PlayerWidget->SetOwningPlayer(this);
			PlayerWidget->AddToPlayerScreen();
		}
	}

	InputComponent->BindAction("Repossess Player", IE_Pressed, this, &AFirstPersonController::RepossessDefaultPawn);
}

void AFirstPersonController::Tick(float DeltaTime)
{
	FHitResult HitResult;
	LineTraceSingleByChannelFromCrosshair(HitResult, 1000);
}

bool AFirstPersonController::LineTraceSingleByChannelFromCenterOfScreen(FHitResult& OutHit, float DistanceToCheck, ECollisionChannel TraceChannel) const
{
	int32 ViewportSizeX, ViewportSizeY;
	GetViewportSize(ViewportSizeX, ViewportSizeY);

	FVector WorldLocation, WorldDirection;
	DeprojectScreenPositionToWorld(ViewportSizeX * 0.5f, ViewportSizeY * 0.5f, WorldLocation, WorldDirection);

	FCollisionQueryParams Params = FCollisionQueryParams(FName(TEXT("")), false, GetOwner());
	if (GetPawn()) 
	{
		Params.AddIgnoredActor(GetPawn());
	}

	return GetWorld()->LineTraceSingleByChannel(
		OutHit,
		WorldLocation,
		WorldLocation + WorldDirection * DistanceToCheck,
		TraceChannel,
		Params
	);
}

bool AFirstPersonController::LineTraceSingleByChannelFromCrosshair(FHitResult& OutHit, float DistanceToCheck, ECollisionChannel TraceChannel) const
{
	int32 ViewportSizeX, ViewportSizeY;
	GetViewportSize(ViewportSizeX, ViewportSizeY);

	FVector WorldLocation, WorldDirection;
	DeprojectScreenPositionToWorld(ViewportSizeX * CrosshairRelativeLocation.X, ViewportSizeY * (1.f - CrosshairRelativeLocation.Y), WorldLocation, WorldDirection);

	FCollisionQueryParams Params = FCollisionQueryParams(FName(TEXT("")), false, GetOwner());
	if (GetPawn())
	{
		Params.AddIgnoredActor(GetPawn());
	}

	return GetWorld()->LineTraceSingleByChannel(
		OutHit,
		WorldLocation,
		WorldLocation + WorldDirection * DistanceToCheck,
		TraceChannel,
		Params
	);
}


void AFirstPersonController::RepossessDefaultPawn()
{
	if (!DefaultPawn) return;

	Possess(DefaultPawn);
}

FVector2D AFirstPersonController::GetCrosshairRelativeLocation() const
{
	return CrosshairRelativeLocation;
}