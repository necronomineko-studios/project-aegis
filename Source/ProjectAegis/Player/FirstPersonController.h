// Copyright Necronomineko Studio

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "FirstPersonController.generated.h"

/**
 * Player Controller for First Person player.
 */
UCLASS()
class PROJECTAEGIS_API AFirstPersonController : public APlayerController
{
	GENERATED_BODY()
	
public:
	virtual void Tick(float DeltaTime) override;

	/**
	 * Linetrace from the center of the screen and forwards in the camera direction.
	 * @param OutHit - Reference to a FHitResult which will contain the results of the linetrace.
	 * @param DistanceToCheck - The length of the linetrace.
	 * @param TraceChannel - The channel to check.
	 * @return Whether or not something was hit by the linetrace.
	 */
	bool LineTraceSingleByChannelFromCenterOfScreen(FHitResult& OutHit, float DistanceToCheck, ECollisionChannel TraceChannel = ECollisionChannel::ECC_Visibility) const;

	/**
	 * Linetrace from the crosshair on the screen and forwards in the camera direction.
	 * @param OutHit - Reference to a FHitResult which will contain the results of the linetrace.
	 * @param DistanceToCheck - The length of the linetrace.
	 * @param TraceChannel - The channel to check.
	 * @return Whether or not something was hit by the linetrace.
	 */
	bool LineTraceSingleByChannelFromCrosshair(FHitResult& OutHit, float DistanceToCheck, ECollisionChannel TraceChannel = ECollisionChannel::ECC_Visibility) const;

	/** Repossesses the default pawn */
	void RepossessDefaultPawn();

	/**
	 * @return the crosshair relative position.
	 */
	UFUNCTION(BlueprintCallable, Category = "UI")
	FVector2D GetCrosshairRelativeLocation() const;

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI", meta = (AllowPrivateAccess = "true"))
	FVector2D CrosshairRelativeLocation = FVector2D(0.5f, 0.5f);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI", meta = (AllowPrivateAccess = "true"))
	TSubclassOf<class UUserWidget> PlayerWidgetClass = nullptr;

	UPROPERTY()
	class UUserWidget* PlayerWidget = nullptr;

	UPROPERTY()
	APawn* DefaultPawn = nullptr;
};
