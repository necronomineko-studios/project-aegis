// Copyright Necronomineko Studio


#include "PossessionIntermediary.h"
#include "GameFramework/Controller.h"

APawn* UPossessionIntermediary::GetPawnToPossess() const
{
	return PawnToPossess;
}

void UPossessionIntermediary::PossessPawn(AController* Controller) const
{
	if (PawnToPossess)
	{
		Controller->Possess(PawnToPossess);
	}
}