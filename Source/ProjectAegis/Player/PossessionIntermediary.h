// Copyright Necronomineko Studio

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PossessionIntermediary.generated.h"

/**
 * Intermediary class that contains a pawn which can be possessed.
 */

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PROJECTAEGIS_API UPossessionIntermediary : public UActorComponent
{
	GENERATED_BODY()

public:
	/**
	 * Gets the pawn this intermediary lets you possess.
	 * @return The pawn this intermediary lets you possess.
	 */
	APawn* GetPawnToPossess() const;

	/**
	 * Used to possess the pawn this intermediary lets you possess.
	 * @param Controller - The controller to possess the pawn.
	 */
	void PossessPawn(class AController* Controller) const;

private:
	UPROPERTY(EditAnywhere, Category = "Possession", meta = (AllowPrivateAccess = "true"))
	APawn* PawnToPossess = nullptr;
		
};
