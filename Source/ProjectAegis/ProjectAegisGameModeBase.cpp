// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectAegisGameModeBase.h"
#include "Engine/World.h"
#include "GameFramework/WorldSettings.h"

void AProjectAegisGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	SetWorldOriginRebasing(bEnableWorldOriginRebasing);
}

void AProjectAegisGameModeBase::SetWorldOriginRebasing(bool bEnabled)
{
	if (UWorld* World = GetWorld())
	{
		if (AWorldSettings* WorldSettings = World->GetWorldSettings())
		{
			WorldSettings->bEnableWorldOriginRebasing = bEnabled;
		}
	}
}