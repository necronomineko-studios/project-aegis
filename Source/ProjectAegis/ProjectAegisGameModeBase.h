// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ProjectAegisGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTAEGIS_API AProjectAegisGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	virtual void BeginPlay() override;

private:
	UFUNCTION(BlueprintCallable, Category = "World Settings")
	void SetWorldOriginRebasing(bool bEnabled);

	UPROPERTY(EditAnywhere, Category = "World Settings", meta = (AllowPrivateAccess = "true"))
	bool bEnableWorldOriginRebasing = true;
};
