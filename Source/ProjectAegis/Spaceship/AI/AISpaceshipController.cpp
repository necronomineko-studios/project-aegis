// Copyright Necronomineko Studio


#include "AISpaceshipController.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BlackboardData.h"
#include "Kismet/GameplayStatics.h"

void AAISpaceshipController::BeginPlay()
{
	Super::BeginPlay();

	SetupBehaviourTree();
}

void AAISpaceshipController::SetupBehaviourTree()
{
	bool bSuccess = false;

	if (BehaviourTree)
	{
		SetupBlackboard();
		bSuccess = RunBehaviorTree(BehaviourTree);
	}

	if(!bSuccess)
	{
		UE_LOG(LogTemp, Error, TEXT("Could not set up Behaviour Tree for: %s"), *GetOwner()->GetName())
	}
}

void AAISpaceshipController::SetupBlackboard()
{
	bool bSuccess = false;

	if (BehaviourTree)
	{
		UBlackboardData* BBData = BehaviourTree->BlackboardAsset;

		if (BBData && UseBlackboard(BBData, Blackboard))
		{
			bSuccess = true;
			Blackboard->SetValueAsObject(FName("Player"), UGameplayStatics::GetPlayerPawn(this, 0));
		}
	}

	if(!bSuccess)
	{
		UE_LOG(LogTemp, Error, TEXT("Could not set up Blackboard Data for: %s"), *GetOwner()->GetName())
	}
	
}
