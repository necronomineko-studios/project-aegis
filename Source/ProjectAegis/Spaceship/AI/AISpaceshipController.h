// Copyright Necronomineko Studio

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "AISpaceshipController.generated.h"

/**
 * Class representing AI controller for the Spaceship.
 */
UCLASS()
class PROJECTAEGIS_API AAISpaceshipController : public AAIController
{
	GENERATED_BODY()
	
public:
	virtual void BeginPlay() override;

protected:
	void SetupBehaviourTree();
	void SetupBlackboard();

private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AI", meta = (AllowPrivateAccess = "true"))
	UBehaviorTree* BehaviourTree = nullptr;
};
