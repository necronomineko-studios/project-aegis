// Copyright Necronomineko Studio


#include "AimAtTarget.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Engine/World.h"
#include "Kismet/KismetMathLibrary.h"
#include "Spaceship/Spaceship.h"


EBTNodeResult::Type UAimAtTarget::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	UBlackboardComponent* BlackboardComp = OwnerComp.GetBlackboardComponent();

	if (BlackboardComp)
	{
		ASpaceship* Self = Cast<ASpaceship>(BlackboardComp->GetValueAsObject(SelfKey.SelectedKeyName));
		APawn* Target = Cast<APawn>(BlackboardComp->GetValueAsObject(TargetKey.SelectedKeyName));
		if (Self && Target)
		{
			AimAt(Self, Target);
			return EBTNodeResult::Succeeded;
		}
	}

	return EBTNodeResult::Failed;
}

void UAimAtTarget::AimAt(ASpaceship* Self, APawn* Target) const
{
	FRotator Rotation = FQuat::Slerp(Self->GetActorRotation().Quaternion(), UKismetMathLibrary::FindLookAtRotation(Self->GetActorLocation(), Target->GetActorLocation()).Quaternion(), 1.f * GetWorld()->DeltaTimeSeconds).Rotator();
	Self->SetActorRotation(Rotation);
}
