// Copyright Necronomineko Studio

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "AimAtTarget.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTAEGIS_API UAimAtTarget : public UBTTaskNode
{
	GENERATED_BODY()

private:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

protected:
	void AimAt(class ASpaceship* Self, APawn* Target) const;

	UPROPERTY(EditAnywhere, Category = "Blackboard")
	struct FBlackboardKeySelector SelfKey;
	UPROPERTY(EditAnywhere, Category = "Blackboard")
	struct FBlackboardKeySelector TargetKey;
};
