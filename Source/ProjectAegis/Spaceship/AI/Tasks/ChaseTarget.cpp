// Copyright Necronomineko Studio


#include "BehaviorTree/BlackboardComponent.h"
#include "ChaseTarget.h"
#include "Engine/World.h"
#include "Spaceship/Spaceship.h"
#include "Spaceship/SpaceshipMovementComponent.h"


EBTNodeResult::Type UChaseTarget::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	UBlackboardComponent* BlackboardComp = OwnerComp.GetBlackboardComponent();

	if (BlackboardComp)
	{
		ASpaceship* Self = Cast<ASpaceship>(BlackboardComp->GetValueAsObject(SelfKey.SelectedKeyName));
		APawn* Target = Cast<APawn>(BlackboardComp->GetValueAsObject(TargetKey.SelectedKeyName));
		if (Self && Target)
		{
			Chase(Self, Target);
			return EBTNodeResult::Succeeded;
		}
	}

	return EBTNodeResult::Failed;
}

void UChaseTarget::Chase(ASpaceship* Self, APawn* Target) const
{
	USpaceshipMovementComponent* MovementComponent = Self->GetSpaceshipMovementComponent();

	if (MovementComponent)
	{
		FVector TargetLocation = Target->GetActorLocation() + -Target->GetActorForwardVector() * TargetDistance;

		if (FVector::Distance(Self->GetActorLocation(), TargetLocation) > AcceptableDeviationDistance)
		{
			FVector Direction = (TargetLocation - Self->GetActorLocation()).GetSafeNormal();
			float ForwardDot = FVector::DotProduct(Self->GetActorForwardVector(), Direction);
			float RightDot = FVector::DotProduct(Self->GetActorRightVector(), Direction);
			float UpDot = FVector::DotProduct(Self->GetActorUpVector(), Direction);

			MovementComponent->AddThrustForwardInput(FMath::Abs(ForwardDot) > 0.5f ? ForwardDot > 0 ? 1.f : -1.f : 0.f);
			MovementComponent->AddThrustRightInput(FMath::Abs(RightDot) > 0.5f ? RightDot > 0 ? 1.f : -1.f : 0.f);
			MovementComponent->AddThrustUpInput(FMath::Abs(UpDot) > 0.5f ? UpDot > 0 ? 1.f : -1.f : 0.f);
		}
	}
}
