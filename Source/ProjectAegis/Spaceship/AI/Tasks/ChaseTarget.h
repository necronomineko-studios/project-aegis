// Copyright Necronomineko Studio

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "ChaseTarget.generated.h"

/**
 * Task for AI spaceship to chase its target.
 */
UCLASS()
class PROJECTAEGIS_API UChaseTarget : public UBTTaskNode
{
	GENERATED_BODY()
	
public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	
protected:
	void Chase(class ASpaceship* Self, APawn* Target) const;

	UPROPERTY(EditAnywhere, Category = "Blackboard")
	float TargetDistance = 10000.f;
	UPROPERTY(EditAnywhere, Category = "Blackboard")
	float AcceptableDeviationDistance = 1000.f;
	UPROPERTY(EditAnywhere, Category = "Blackboard")
	struct FBlackboardKeySelector SelfKey;
	UPROPERTY(EditAnywhere, Category = "Blackboard")
	struct FBlackboardKeySelector TargetKey;
};
