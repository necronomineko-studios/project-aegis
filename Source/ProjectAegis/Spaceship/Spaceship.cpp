// Copyright Necronomineko Studio


#include "Spaceship.h"
#include "Components/InputComponent.h"
#include "Engine/World.h"
#include "GameFramework/InputSettings.h"
#include "SpaceshipMovementComponent.h"
#include "Weapons/SpaceshipWeaponComponent.h"


// Sets default values
ASpaceship::ASpaceship()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SpaceshipMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Spaceship Mesh"));
	SetRootComponent(SpaceshipMesh);
	SpaceshipMesh->SetSimulatePhysics(true);
	SpaceshipMesh->SetEnableGravity(false);
	SpaceshipMesh->SetLinearDamping(0.f);
	SpaceshipMesh->SetAngularDamping(4.f);

	MovementComponent = CreateDefaultSubobject<USpaceshipMovementComponent>(TEXT("Spaceship Movement Component"));
}

void ASpaceship::BeginPlay()
{
	Super::BeginPlay();

	ActivateWeapons(); // TODO: Refactor this into input.
}

USpaceshipMovementComponent* ASpaceship::GetSpaceshipMovementComponent() const
{
	return MovementComponent;
}

// Called to bind functionality to input
void ASpaceship::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Setup movement input.
	PlayerInputComponent->BindAxis("Ship Forward", MovementComponent, &USpaceshipMovementComponent::AddThrustForwardInput);
	PlayerInputComponent->BindAxis("Ship Right", MovementComponent, &USpaceshipMovementComponent::AddThrustRightInput);
	PlayerInputComponent->BindAxis("Ship Up", MovementComponent, &USpaceshipMovementComponent::AddThrustUpInput);
	PlayerInputComponent->BindAxis("Ship Pitch", MovementComponent, &USpaceshipMovementComponent::AddRotationPitchInput);
	PlayerInputComponent->BindAxis("Ship Yaw", MovementComponent, &USpaceshipMovementComponent::AddRotationYawInput);
	PlayerInputComponent->BindAxis("Ship Roll", MovementComponent, &USpaceshipMovementComponent::AddRotationRollInput);

	// Setup weapon input.
	PlayerInputComponent->BindAction("Fire Primary Weapons", EInputEvent::IE_Pressed, this, &ASpaceship::StartFirePrimaryWeapons);
	PlayerInputComponent->BindAction("Fire Primary Weapons", EInputEvent::IE_Released, this, &ASpaceship::StopFirePrimaryWeapons);
}

void ASpaceship::AddPrimaryWeapon(USpaceshipWeaponComponent* Weapon)
{
	PrimaryWeapons.Add(Weapon);
}

void ASpaceship::ActivateWeapons()
{
	for (USpaceshipWeaponComponent* Weapon : PrimaryWeapons)
	{
		if (Weapon)
		{
			Weapon->ActivateWeapon();
		}
	}
}

void ASpaceship::DeactivateWeapons()
{
	for (USpaceshipWeaponComponent* Weapon : PrimaryWeapons)
	{
		if (Weapon)
		{
			Weapon->DeactivateWeapon();
		}
	}
}

void ASpaceship::StartFirePrimaryWeapons()
{
	for (USpaceshipWeaponComponent* Weapon : PrimaryWeapons)
	{
		if (Weapon != nullptr)
		{
			Weapon->StartFire();
		}
	}
}

void ASpaceship::StopFirePrimaryWeapons()
{
	for (USpaceshipWeaponComponent* Weapon : PrimaryWeapons)
	{
		if (Weapon != nullptr)
		{
			Weapon->StopFire();
		}
	}
}
