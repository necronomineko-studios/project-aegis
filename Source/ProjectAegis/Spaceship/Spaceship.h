// Copyright Necronomineko Studio

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Spaceship.generated.h"

// Forward declarations
class UPhysicsThrusterComponent;

UCLASS()
class PROJECTAEGIS_API ASpaceship : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASpaceship();

	virtual void BeginPlay() override;
	class USpaceshipMovementComponent* GetSpaceshipMovementComponent() const;

protected:
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Weapon functions
	UFUNCTION(BlueprintCallable, Category = "Weapons")
	void AddPrimaryWeapon(class USpaceshipWeaponComponent* Weapon);
	UFUNCTION(BlueprintCallable, Category = "Weapons")
	void ActivateWeapons();
	UFUNCTION(BlueprintCallable, Category = "Weapons")
	void DeactivateWeapons();
	void StartFirePrimaryWeapons();
	void StopFirePrimaryWeapons();

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Mesh", meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* SpaceshipMesh = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement", meta = (AllowPrivateAccess = "true"))
	class USpaceshipMovementComponent* MovementComponent = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapons", meta = (AllowPrivateAccess = "true"))
	TArray<class USpaceshipWeaponComponent*> PrimaryWeapons;
};
