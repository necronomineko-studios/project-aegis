// Copyright Necronomineko Studio


#include "Components/StaticMeshComponent.h"
#include "SpaceshipMovementComponent.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/Controller.h"


USpaceshipMovementComponent::USpaceshipMovementComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void USpaceshipMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	PawnOwner = Cast<APawn>(GetOwner());
	if (!PawnOwner) return;

	UStaticMeshComponent* Mesh = Cast<UStaticMeshComponent>(PawnOwner->GetRootComponent());
	Mesh->SetPhysicsMaxAngularVelocityInDegrees(MaxAngularVelocity);
}

void USpaceshipMovementComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	if (!PawnOwner) return;

	const AController* Controller = PawnOwner->GetController();
	if (Controller && Controller->IsLocalController())
	{
		UpdatePawnLinearVelocity(DeltaTime);
		UpdatePawnAngularVelocity(DeltaTime);
	}
}

void USpaceshipMovementComponent::AddThrustForwardInput(float Value)
{
	ThrustInput.X += Value;
}

void USpaceshipMovementComponent::AddThrustRightInput(float Value)
{
	ThrustInput.Y += Value;
}

void USpaceshipMovementComponent::AddThrustUpInput(float Value)
{
	ThrustInput.Z += Value;
}

void USpaceshipMovementComponent::AddRotationPitchInput(float Value)
{
	RotationInput.Pitch += Value;
}

void USpaceshipMovementComponent::AddRotationRollInput(float Value)
{
	RotationInput.Roll += Value;
}

void USpaceshipMovementComponent::AddRotationYawInput(float Value)
{
	RotationInput.Yaw += Value;
}

void USpaceshipMovementComponent::AddRotationInput(FRotator Rotation)
{
	RotationInput += Rotation;
}

FVector USpaceshipMovementComponent::ConsumeThrustInput()
{
	FVector VectorToReturn = FVector(
		FMath::Clamp(ThrustInput.X, -1.f, 1.f),
		FMath::Clamp(ThrustInput.Y, -1.f, 1.f),
		FMath::Clamp(ThrustInput.Z, -1.f, 1.f)
	);
	ThrustInput = FVector::ZeroVector;
	return VectorToReturn;
}

void USpaceshipMovementComponent::UpdatePawnLinearVelocity(float DeltaTime)
{
	FVector Input = ConsumeThrustInput();

	if (!PawnOwner) return;

	FVector Forward = PawnOwner->GetActorForwardVector() * Input.X * ThrustForwardAcceleration * LinearScale * DeltaTime;
	FVector Right = PawnOwner->GetActorRightVector() * Input.Y * ThrustRightAcceleration * LinearScale * DeltaTime;
	FVector Up = PawnOwner->GetActorUpVector() * Input.Z * ThrustUpAcceleration * LinearScale * DeltaTime;

	UStaticMeshComponent* Mesh = Cast<UStaticMeshComponent>(PawnOwner->GetRootComponent());
	if (Mesh)
	{
		FVector OldLinearVelocity = Mesh->GetPhysicsLinearVelocity();
		FVector LinearDecelerationDelta = OldLinearVelocity.GetSafeNormal() * LinearDeceleration * LinearScale * DeltaTime;
		FVector DeceleratedLinearVelocity = LinearDecelerationDelta.Size() >= OldLinearVelocity.Size() ? FVector::ZeroVector : OldLinearVelocity - LinearDecelerationDelta;

		Mesh->SetPhysicsLinearVelocity((DeceleratedLinearVelocity + Forward + Right + Up).GetClampedToMaxSize(MaxLinearVelocity * LinearScale));
	}
}

FRotator USpaceshipMovementComponent::ConsumeRotationInput()
{
	FRotator RotatorToReturn = FRotator(
		FMath::Clamp(RotationInput.Pitch, -1.f, 1.f),
		FMath::Clamp(RotationInput.Yaw, -1.f, 1.f),
		FMath::Clamp(RotationInput.Roll, -1.f, 1.f)
	);
	RotationInput = FRotator::ZeroRotator;
	return RotatorToReturn;
}

void USpaceshipMovementComponent::UpdatePawnAngularVelocity(float DeltaTime)
{
	FRotator Input = ConsumeRotationInput();

	if (!PawnOwner) return;

	// Convert local rotation to world rotation.
	FVector Pitch = PawnOwner->GetActorRightVector() * Input.Pitch * PitchAcceleration * DeltaTime;
	FVector Yaw = PawnOwner->GetActorUpVector() * Input.Yaw * YawAcceleration * DeltaTime;
	FVector Roll = PawnOwner->GetActorForwardVector() * Input.Roll * RollAcceleration * DeltaTime;

	UStaticMeshComponent* Mesh = Cast<UStaticMeshComponent>(PawnOwner->GetRootComponent());
	if (Mesh)
	{
		Mesh->AddTorque(Pitch + Yaw + Roll, NAME_None, true);
	}
}
