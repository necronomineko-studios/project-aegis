// Copyright Necronomineko Studio

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SpaceshipMovementComponent.generated.h"

/**
 * SpaceshipMovementComponent adds rotation support to FloatingPawnMovement.
 */
UCLASS(ClassGroup = Movement, meta = (BlueprintSpawnableComponent))
class PROJECTAEGIS_API USpaceshipMovementComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	USpaceshipMovementComponent();

	virtual void BeginPlay() override;

	// Thrust Input in local space.
	UFUNCTION(BlueprintCallable, Category = "Thrust")
	void AddThrustForwardInput(float Value);
	UFUNCTION(BlueprintCallable, Category = "Thrust")
	void AddThrustRightInput(float Value);
	UFUNCTION(BlueprintCallable, Category = "Thrust")
	void AddThrustUpInput(float Value);

	// Rotation Input in local space.
	UFUNCTION(BlueprintCallable, Category = "Rotation")
	void AddRotationPitchInput(float Value);
	UFUNCTION(BlueprintCallable, Category = "Rotation")
	void AddRotationRollInput(float Value);
	UFUNCTION(BlueprintCallable, Category = "Rotation")
	void AddRotationYawInput(float Value);
	UFUNCTION(BlueprintCallable, Category = "Rotation")
	void AddRotationInput(FRotator Rotation);

protected:
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	/**
	 * Returns the ThrustInput and clears it.
	 * @return the ThrustInput.
	 */
	UFUNCTION()
	FVector ConsumeThrustInput();

	/** Update the linear velocity of the owner based on input. */
	UFUNCTION()
	void UpdatePawnLinearVelocity(float DeltaTime);

	/**
	 * Returns the RotationInput and clears it.
	 * @return the RotationInput;
	 */
	UFUNCTION()
	FRotator ConsumeRotationInput();

	/** Update the angular velocity of the owner based on input. */
	UFUNCTION()
	void UpdatePawnAngularVelocity(float DeltaTime);
	
private:
	UPROPERTY(EditAnywhere, Category = "Linear Movement", meta = (AllowPrivateAccess = "true"))
	float ThrustForwardAcceleration = 4.f;
	UPROPERTY(EditAnywhere, Category = "Linear Movement", meta = (AllowPrivateAccess = "true"))
	float ThrustRightAcceleration = 1.f;
	UPROPERTY(EditAnywhere, Category = "Linear Movement", meta = (AllowPrivateAccess = "true"))
	float ThrustUpAcceleration = 1.f;
	UPROPERTY(EditAnywhere, Category = "Linear Movement", meta = (AllowPrivateAccess = "true"))
	float MaxLinearVelocity = 100.f;
	UPROPERTY(EditAnywhere, Category = "Linear Movement", meta = (AllowPrivateAccess = "true"))
	float LinearDeceleration = 2.f;
	UPROPERTY(EditAnywhere, Category = "Linear Movement", meta = (AllowPrivateAccess = "true"))
	float LinearScale = 1000.f;
	UPROPERTY()
	FVector ThrustInput = FVector::ZeroVector;

	UPROPERTY(EditAnywhere, Category = "Angular Movement", meta = (AllowPrivateAccess = "true"))
	float PitchAcceleration = 250.f;
	UPROPERTY(EditAnywhere, Category = "Angular Movement", meta = (AllowPrivateAccess = "true"))
	float YawAcceleration = 100.f;
	UPROPERTY(EditAnywhere, Category = "Angular Movement", meta = (AllowPrivateAccess = "true"))
	float RollAcceleration = 300.f;
	UPROPERTY(EditAnywhere, Category = "Angular Movement", meta = (AllowPrivateAccess = "true"))
	float MaxAngularVelocity = 30.f;
	UPROPERTY(EditAnywhere, Category = "Angular Movement", meta = (AllowPrivateAccess = "true"))
	float AngularDeceleration = 50.f;
	UPROPERTY()
	FRotator RotationInput = FRotator::ZeroRotator;

	UPROPERTY()
	APawn* PawnOwner = nullptr;
};
