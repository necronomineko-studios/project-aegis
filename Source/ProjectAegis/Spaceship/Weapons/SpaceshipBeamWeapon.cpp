// Copyright Necronomineko Studio


#include "Engine/World.h"
#include "GameFramework/DamageType.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "SpaceshipBeamWeapon.h"
#include "TimerManager.h"


void USpaceshipBeamWeapon::BeginPlay()
{
	Super::BeginPlay();

	if (BeamParticleSystem)
	{
		BeamParticleSystemComponent = UGameplayStatics::SpawnEmitterAtLocation(this, BeamParticleSystem, GetSocketLocation("FireOrigin"), GetSocketRotation("FireOrigin"), FVector::OneVector, false, EPSCPoolMethod::ManualRelease, false);
		BeamParticleSystemComponent->AttachToComponent(this, FAttachmentTransformRules::KeepRelativeTransform, FName("FireOrigin"));
	}
}

void USpaceshipBeamWeapon::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	BeamParticleSystemComponent->Complete();
	BeamParticleSystemComponent->ReleaseToPool();
	GetWorld()->GetTimerManager().ClearTimer(BeamHandle);
}

void USpaceshipBeamWeapon::StartFire()
{
	Super::StartFire();
	
	BeamParticleSystemComponent->ActivateSystem();
	GetWorld()->GetTimerManager().SetTimer(BeamHandle, this, &USpaceshipBeamWeapon::UpdateBeamSourceAndTarget, BeamAimRate, true, 0.f);
}

void USpaceshipBeamWeapon::StopFire()
{
	Super::StopFire();

	GetWorld()->GetTimerManager().ClearTimer(BeamHandle);
	BeamParticleSystemComponent->Complete();
}

void USpaceshipBeamWeapon::FireWeapon()
{
	FHitResult BeamHit;
	bool bBeamHit = FindBeamHit(BeamHit);

	if (bBeamHit)
	{
		DealDamageToActor(BeamHit.GetActor());
	}
}

void USpaceshipBeamWeapon::UpdateBeamSourceAndTarget()
{
	FHitResult BeamHit;
	bool bBeamHit = FindBeamHit(BeamHit);

	BeamParticleSystemComponent->SetBeamSourcePoint(0, GetSocketLocation("FireOrigin"), 0);

	if (bBeamHit)
	{
		BeamParticleSystemComponent->SetBeamTargetPoint(0, BeamHit.Location, 0);
	}
	else
	{
		BeamParticleSystemComponent->SetBeamTargetPoint(0, GetSocketLocation("FireOrigin") + GetSocketRotation("FireOrigin").Vector() * MaxDistance, 0);
	}
}

bool USpaceshipBeamWeapon::FindBeamHit(FHitResult& OutHit) const
{
	return GetWorld()->LineTraceSingleByChannel(
		OutHit,
		GetSocketLocation(FName("FireOrigin")),
		GetSocketLocation(FName("FireOrigin")) + GetForwardVector() * MaxDistance,
		ECollisionChannel::ECC_Visibility
	);
}

void USpaceshipBeamWeapon::DealDamageToActor(AActor* ActorToDamage) const
{
	if (!ActorToDamage || !Cast<APawn>(GetOwner())) return;

	static TSubclassOf<UDamageType> const ValidDamageTypeClass = TSubclassOf<UDamageType>(UDamageType::StaticClass());
	static FDamageEvent DamageEvent(ValidDamageTypeClass);

	ActorToDamage->TakeDamage(DamagePerDamageTick, DamageEvent, Cast<APawn>(GetOwner())->GetController(), GetOwner());
}