// Copyright Necronomineko Studio

#pragma once

#include "CoreMinimal.h"
#include "Spaceship/Weapons/SpaceshipWeaponComponent.h"
#include "SpaceshipBeamWeapon.generated.h"

/**
 * Beam specialisation of Spaceship Weapon Component.
 */
UCLASS()
class PROJECTAEGIS_API USpaceshipBeamWeapon : public USpaceshipWeaponComponent
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	/** Overriden function to start firing the beam weapon. */
	virtual void StartFire() override;
	/** Overriden function to stop firing the beam weapon. */
	virtual void StopFire() override;

protected:
	/** Overriden function to fire the beam weapon. */
	virtual void FireWeapon() override;

	/** Function to update the beam effect's source and target points. */
	void UpdateBeamSourceAndTarget();

	/**
	 * Line trace to find if the beam hit something.
	 * @param OutHit - Output of the results of the check.
	 * @return whether or not something was hit.
	 */
	bool FindBeamHit(FHitResult& OutHit) const;

	/**
	 * Deal DamagePerDamageTick amount of damage to an actor.
	 * @param ActorToDamage - The actor to deal damage to.
	 */
	void DealDamageToActor(AActor* ActorToDamage) const;

private:
	/** The beam Particle System. */
	UPROPERTY(EditDefaultsOnly, Category = "Weapon Settings", meta = (AllowPrivateAccess = "true"))
	UParticleSystem* BeamParticleSystem = nullptr;

	/** The Particle System Component for the beam. */
	UPROPERTY()
	UParticleSystemComponent* BeamParticleSystemComponent = nullptr;

	/** The amount of damage the weapon inflicts per tick of the damage. */
	UPROPERTY(EditDefaultsOnly, Category = "Weapon Settings", meta = (AllowPrivateAccess = "true"))
	float DamagePerDamageTick = 1.f;

	/** Seconds between updating beam source and target locations. */
	UPROPERTY()
	float BeamAimRate = 1.f/60.f;

	/** Timer handle for updating the beam. */
	UPROPERTY()
	FTimerHandle BeamHandle;
};
