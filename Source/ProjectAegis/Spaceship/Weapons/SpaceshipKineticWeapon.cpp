// Copyright Necronomineko Studio


#include "Artillery/Projectile.h"
#include "SpaceshipKineticWeapon.h"
#include "Spaceship/Spaceship.h"


void USpaceshipKineticWeapon::FireWeapon()
{
	if (ProjectileClass)
	{
		AProjectile* Projectile = GetWorld()->SpawnActor<AProjectile>(ProjectileClass, GetSocketLocation(FName("FireOrigin")), GetSocketRotation(FName("FireOrigin")));
		FVector Velocity = GetOwner()->GetVelocity() + GetSocketRotation("FireOrigin").Vector() * ProjectileSpeed;
		Projectile->LaunchProjectile(Velocity);
		
		Cast<UPrimitiveComponent>(GetOwner()->GetRootComponent())->IgnoreActorWhenMoving(Projectile, true);
		Cast<UPrimitiveComponent>(Projectile->GetRootComponent())->IgnoreActorWhenMoving(GetOwner(), true);
	}
}
