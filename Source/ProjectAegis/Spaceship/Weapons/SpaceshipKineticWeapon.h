// Copyright Necronomineko Studio

#pragma once

#include "CoreMinimal.h"
#include "Spaceship/Weapons/SpaceshipWeaponComponent.h"
#include "SpaceshipKineticWeapon.generated.h"

/**
 * Kinetic specialisation of Spaceship Weapon Component.
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PROJECTAEGIS_API USpaceshipKineticWeapon : public USpaceshipWeaponComponent
{
	GENERATED_BODY()

protected:
	virtual void FireWeapon() override;

private:
	UPROPERTY(EditDefaultsOnly, Category = "Weapon Settings", meta = (AllowPrivateAccess = "true"))
	TSubclassOf<class AProjectile> ProjectileClass = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon Settings", meta = (AllowPrivateAccess = "true"))
	float ProjectileSpeed = 40000.f;
};
