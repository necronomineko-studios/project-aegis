// Copyright Necronomineko Studio


#include "Engine/World.h"
#include "Kismet/KismetMathLibrary.h"
#include "Player/FirstPersonController.h"
#include "SpaceshipWeaponComponent.h"
#include "TimerManager.h"


void USpaceshipWeaponComponent::ActivateWeapon()
{
	bWeaponActive = true;
	GetWorld()->GetTimerManager().SetTimer(AimingHandle, this, &USpaceshipWeaponComponent::AimAtCrosshair, AimRate, true, 0.f);
}

void USpaceshipWeaponComponent::DeactivateWeapon()
{
	GetWorld()->GetTimerManager().ClearTimer(AimingHandle);
	StopFire();
	bWeaponActive = false;
}

void USpaceshipWeaponComponent::StartFire()
{
	if (!bWeaponActive) return;

	GetWorld()->GetTimerManager().SetTimer(FiringHandle, this, &USpaceshipWeaponComponent::FireWeapon, FireRate, true, 0.f);
}

void USpaceshipWeaponComponent::StopFire()
{
	if (!bWeaponActive) return;

	GetWorld()->GetTimerManager().ClearTimer(FiringHandle);
}

void USpaceshipWeaponComponent::AimAtCrosshair()
{
	AFirstPersonController* PlayerController = Cast<AFirstPersonController>(Cast<APawn>(GetOwner())->GetController());
	if (PlayerController)
	{
		FHitResult HitResult;
		bool bHasHit = PlayerController->LineTraceSingleByChannelFromCrosshair(HitResult, MaxDistance);

		FVector LocationToAimAt = bHasHit ? HitResult.Location : HitResult.TraceEnd;
		
		FRotator Rotation = UKismetMathLibrary::FindLookAtRotation(GetComponentLocation(), LocationToAimAt);
		SetWorldRotation(Rotation);
	}
}

void USpaceshipWeaponComponent::FireWeapon()
{
	unimplemented();
}

void USpaceshipWeaponComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	GetWorld()->GetTimerManager().ClearTimer(FiringHandle);
	GetWorld()->GetTimerManager().ClearTimer(AimingHandle);
}