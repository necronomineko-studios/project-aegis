// Copyright Necronomineko Studio

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "SpaceshipWeaponComponent.generated.h"

/**
 * Class representing a weapon for spaceships.
 */
UCLASS()
class PROJECTAEGIS_API USpaceshipWeaponComponent : public UStaticMeshComponent
{
	GENERATED_BODY()

public:
	/** Activates the weapon, allowing it to update aiming and fire. */
	UFUNCTION()
	virtual void ActivateWeapon();
	/** Deactivates the weapon, preventing it from updating aiming and being able to fire. */
	UFUNCTION()
	virtual void DeactivateWeapon();
	/** @return Whether or not the weapon is active. */
	UFUNCTION()
	virtual bool GetWeaponActive() const { return bWeaponActive; }

	/** Starts firing the weapon. */
	UFUNCTION()
	virtual void StartFire();
	/** Stops firing the weapon. */
	UFUNCTION()
	virtual void StopFire();

protected:
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	/** Aims the weapon towards the crosshair. */
	UFUNCTION()
	virtual void AimAtCrosshair();

	/** Functions that represents the action of firing the weapon. */
	UFUNCTION()
	virtual void FireWeapon();

	/** Max distance to focus aiming on. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon Settings", meta = (AllowPrivateAccess = "true"))
	float MaxDistance = 100000.f;

private:
	/** Seconds between shots. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon Settings", meta = (AllowPrivateAccess = "true"))
	float FireRate = 0.5f;
	/** Seconds between aim updates. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon Settings", meta = (AllowPrivateAccess = "true"))
	float AimRate = 1.f / 60.f;

	/** Timer to handle firing. */
	UPROPERTY()
	FTimerHandle FiringHandle;
	/** Timer to handle aiming. */
	UPROPERTY()
	FTimerHandle AimingHandle;

	/** Cache whether the weapon is active or not. */
	UPROPERTY()
	bool bWeaponActive = false;
};
